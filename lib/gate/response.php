<?php

namespace Mytest\Apicrud\Gate;

use Bitrix\Main\Engine\Response\AjaxJson;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Error;
use Bitrix\Main\HttpResponse;
use Bitrix\Main\Type\DateTime;

class Response
{
    const API_VERSION = '0.0.8';
    
    private static $instance;
    private $status;
    private $response;
    private $httpResponse;

    private function __construct()
    {
        $this->response = [
            'api' => [
                'version' => static::API_VERSION,
            ],
        ];
    }
    
    private function __clone() {
        
    }
    
    private function __wakeup() {
        
    }

    public static function getInstance(): Response
    {
        if(!isset(self::$instance)){
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function init(HttpResponse $httpResponse): Response
    {
        if(isset($this->httpResponse)){
            return $this;
        }
        
        $this->httpResponse = $httpResponse;
        $this->httpResponse->addHeader('Cache-Control', 'no-store, no-cache, must-revalidate');
        $this->httpResponse->addHeader('Pragma', 'no-cache');
        $this->httpResponse->addHeader('Content-Type', 'application/json; charset=UTF-8');
        
        $this->setStatus(200);
            
        $objDateTime = new DateTime();
        $this->set('timestamp', $objDateTime->getTimestamp());
        
        return $this;
    }

    public function setStatus(int $code): Response
    {
        $this->status = $code;
        
        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function get(): array
    {
        return $this->response;
    }

    public function set(string $key, $value): Response
    {
        $this->response[$key] = $value;
        
        return $this;
    }

    public function endSuccess()
    {
        $this->httpResponse->setStatus($this->getStatus());
        $this->httpResponse->flush(AjaxJson::createSuccess($this->get())->getContent());
    }

    public function endError(int $status, string $message)
    {
        $this->httpResponse->setStatus($status);
        
        $errorCollection = new ErrorCollection();
        $errorCollection[] = new Error($message);
        
        $this->httpResponse->flush(AjaxJson::createError($errorCollection)->getContent());
    }
}

