<?php

namespace Mytest\Apicrud\Gate;

use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Main\SystemException;
use Bitrix\Currency\CurrencyManager;
use Bitrix\Catalog\ProductTable;
use Bitrix\Sale;
use Mytest\Apicrud\Gate\Response;

class Actions
{
    public static function add()
    {
        $request = Context::getCurrent()->getRequest();
        
        $productId = (int) $request->get('productId');
        $quantity = (int) $request->get('quantity');
        
        if( $productId < 1 || $quantity < 1 ){
            Response::getInstance()->setStatus(400);
            throw new SystemException('Input format error');
        }
        
        Loader::includeModule('sale');
        
        $rsElements = ProductTable::getList([
            'select' => [
                'TYPE',
            ],
            'filter' => [
                '=ID' => $productId,
            ],
        ]);
        
        if ($arElement = $rsElements->fetch()) {
            
            $allowTypes = [
                ProductTable::TYPE_PRODUCT,
                ProductTable::TYPE_OFFER,
            ];
            
            if (!in_array($arElement['TYPE'], $allowTypes)) {
                Response::getInstance()->setStatus(400);
                throw new SystemException(sprintf('Product type error (type = %s)', $arElement['TYPE']));
            }
        
            $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Context::getCurrent()->getSite());

            $item = $basket->getExistsItem('catalog', $productId);

            if ($item) {
                
                $item->setField('QUANTITY', $item->getQuantity() + $quantity);

                Response::getInstance()->setStatus(200);
            }
            else {
                $item = $basket->createItem('catalog', $productId);

                $item->setFields([
                    'QUANTITY' => $quantity,
                    'CURRENCY' => CurrencyManager::getBaseCurrency(),
                    'LID' => Context::getCurrent()->getSite(),
                    'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
                ]);

                Response::getInstance()->setStatus(201);
            }

            $basket->save();
            
            Response::getInstance()->set('result', [
                'quantity' => $item->getQuantity(),
                'id' => $item->getId(),
            ]);
            
            return;
        }
        
        Response::getInstance()->setStatus(404);
        throw new SystemException('Product not found');
    }
    
    public static function change()
    {
        $request = Context::getCurrent()->getRequest();
        
        $productId = (int) $request->get('productId');
        $quantity = (int) $request->get('quantity');
        
        if( $productId < 1 || $quantity < 1 ){
            Response::getInstance()->setStatus(400);
            throw new SystemException('Input format error');
        }
        
        Loader::includeModule('sale');
        
        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Context::getCurrent()->getSite());
        
        $item = $basket->getExistsItem('catalog', $productId);
        
        if (!$item) {
            Response::getInstance()->setStatus(404);
            throw new SystemException('Item not found');
        }
        
        $item->setField('QUANTITY', $quantity);
        
        $basket->save();
            
        Response::getInstance()->set('result', [
            'quantity' => $item->getQuantity(),
            'id' => $item->getId(),
        ]);
        
        Response::getInstance()->setStatus(200);
    }
    
    public static function remove()
    {
        $request = Context::getCurrent()->getRequest();
        
        $productId = (int) $request->get('productId');
        
        if( $productId < 1 ){
            Response::getInstance()->setStatus(400);
            throw new SystemException('Input format error');
        }
        
        Loader::includeModule('sale');
        
        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Context::getCurrent()->getSite());
        
        $item = $basket->getExistsItem('catalog', $productId);
        
        if ($item) {
            $item->delete();
            $basket->save();
        }
        
        $arItems = [];
        
        foreach ($basket as $basketItem) {
            $arItems[] = [
                'id' => $basketItem->getId(),
                'productId' => $basketItem->getField('PRODUCT_ID'),
                'name' => $basketItem->getField('NAME'),
                'quantity' => $basketItem->getQuantity(),
            ];
        }
        
        Response::getInstance()->set('result', $arItems);
        
        Response::getInstance()->setStatus(200);
    }
    
    public static function list()
    {
        Loader::includeModule('sale');
        
        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Context::getCurrent()->getSite());
        
        $arItems = [];
        
        foreach ($basket as $basketItem) {
            $arItems[] = [
                'id' => $basketItem->getId(),
                'productId' => $basketItem->getField('PRODUCT_ID'),
                'name' => $basketItem->getField('NAME'),
                'quantity' => $basketItem->getQuantity(),
            ];
        }
        
        Response::getInstance()->set('result', $arItems);
    }
}