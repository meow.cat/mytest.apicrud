<?php

use Bitrix\Main\UrlRewriter;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

Loc::loadMessages(__FILE__);

class Mytest_Apicrud extends CModule
{
    public function __construct()
    {
        $arModuleVersion = include __DIR__ . '/version.php';
        
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        
        $this->MODULE_ID = 'mytest.apicrud';
        
        $this->MODULE_NAME = Loc::getMessage('MYTEST_APICRUD_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('MYTEST_APICRUD_MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = Loc::getMessage('MYTEST_APICRUD_MODULE_PARTNER_NAME');
    }
    
    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        
        if (preg_match('/\/local\/modules\/[^\/]+\/install$/', __DIR__)) {
            $moduleDir = 'local';
        }
        else{
            $moduleDir = 'bitrix';
        }
        
        $path = '/' . $moduleDir . '/modules/' . $this->MODULE_ID . '/gate.php';
        
        UrlRewriter::add(
            \CSite::GetDefSite(),
            [
                'CONDITION' => '#^/mytest\.apicrud/([^\/]+)/(\d+)/?(\?.*)?$#',
                'RULE' => 'method=$1&productId=$2',
                'ID' => $this->MODULE_ID,
                'PATH' => $path,
                'SORT' => 8,
            ]
        );

        UrlRewriter::add(
            \CSite::GetDefSite(),
            [
                'CONDITION' => '#^/mytest\.apicrud/([^\/]+)/?(\?.*)?$#',
                'RULE' => 'method=$1',
                'ID' => $this->MODULE_ID,
                'PATH' => $path,
                'SORT' => 8,
            ]
        );

        UrlRewriter::add(
            \CSite::GetDefSite(),
            [
                'CONDITION' => '#^/mytest\.apicrud/?#',
                'RULE' => '',
                'ID' => $this->MODULE_ID,
                'PATH' => $path,
                'SORT' => 8,
            ]
        );
    }
    
    public function doUninstall()
    {
        $this->uninstallDB();
        ModuleManager::unRegisterModule($this->MODULE_ID);
        
        UrlRewriter::delete(
            \CSite::GetDefSite(),
            [
                'ID' => $this->MODULE_ID,
            ]
        );
    }
}