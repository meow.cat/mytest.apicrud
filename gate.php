<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Context;
use Bitrix\Main\SystemException;
use Bitrix\Main\HttpResponse;
use Mytest\Apicrud\Gate\Actions;
use Mytest\Apicrud\Gate\Response;

require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

Loader::includeModule('mytest.apicrud');

$response = Response::getInstance();
$response->init(new HttpResponse());

$request = Context::getCurrent()->getRequest();
$method = (string) $request->get('method');

if ($method != '') {

    if (method_exists('\Mytest\Apicrud\Gate\Actions', $method)) {
        
        try {
            Actions::$method();
            $response->endSuccess();
        }
        catch (SystemException $exception) {
            
            $status = $response->getStatus();
            
            if($status < 400){
                $status = 500;
            }
            
            $response->endError($status, $exception->getMessage());
        }
        
        return;
    }
}

$response->endError(404, 'Method not found');
